// 1. GET ELEMENT REFERENCES

const colorPicker = document.getElementById("picker"); // TODO: Write code to obtain the colour picker off the DOM
const opacityPicker = document.getElementById("slider"); // TODO: Write code to obtain the opacity picker off the DOM
const target = document.getElementById("target"); // TODO: Write code to obtain the target div element off the DOM

// 2. EVENT LISTENERS

colorPicker.addEventListener("input", function(event) {
  // This event listener will be run whenever the colour picker's value changes.
  // Inside this event listener, you can find the most recently chosen colour at "event.target.value"

  target.style.backgroundColor = event.target.value;// TODO: replace this note with a line of code that sets the target's background colour to the value found at event.target.value.

});

opacityPicker.addEventListener("input", function(event) {
  // This event listener will be run whenever the opacity picker's value changes.
  // Inside this event listener, you can find the most recently chosen opacity at "event.target.value"

  target.style.opacity = event.target.value;// TODO: replace this note with a line of code that sets the target's opacity to the value found at event.target.value

});

// 3. INITIAL PAGE SETUP

target.style.backgroundColor = colorPicker.value;// TODO: replace this note with a line of code that sets the target's background colour to the colour that is.
target.style.opacity = opacityPicker.value;// TODO: replace this note with a line of code that sets the target's opacity to the opacity
